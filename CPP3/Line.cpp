/*************************************************************************
 Line  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <Line> (fichier Line.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Line.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Line::move(long dx, long dy) {
    
    if (!checkOverflow(p1, dx, dy) || !checkOverflow(p2, dx, dy)) {
        return false;
    }
    
    p1.x += dx;
    p1.y += dy;
    
    p2.x += dx;
    p2.y += dy;
    
    return true;
}

string Line::description() {
    stringstream command;
    command << "L " << name << " " << p1.x << " " << p1.y << " "<<p2.x<<" "<<p2.y;
    return command.str();
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Line::Line(string _name, Point first, Point second )
// Algorithme :
//
{
    name=_name;
    p1=first;
    p2=second;
    
} //----- Fin de Line

Line::~Line ( )
// Algorithme :
//
{
    
} //----- Fin de ~Line


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

