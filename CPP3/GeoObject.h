/*************************************************************************
                           GeoObject  -  Objet geometrique
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <GeoObject> (fichier GeoObject.h) ------
#if ! defined ( __TP__GeoObject__)
#define __TP__GeoObject__

//--------------------------------------------------- Interfaces utilisées
#include <string>
#include <sstream>

using namespace std;

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 
struct Point {
    long x;
    long y;
};

//------------------------------------------------------------------------ 
// Rôle de la classe <GeoObject>
//
//
//------------------------------------------------------------------------ 

class GeoObject
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    

    virtual bool move(long dx, long dy) = 0;
    // Mode d'emploi :
    // Permet de déplacer un objet avec x sur l'axe X et y sur l'axe Y
    // Contrat :
    //
    
    virtual string description() = 0;
    // Mode d'emploi :
    // Descripteur de l'objet
    // Contrat :
    //
    
    string getName();
    // Mode d'emploi :
    // Accesseur du nom de l'objet
    // Contrat :
    //
    
    bool checkOverflow(Point pt, long dx, long dy);
    // Mode d'emploi :
    // Vérifie l'overflow lors des operations de deplacement
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

    GeoObject();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    GeoObject(string _name);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~GeoObject ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    
    string name;
    
};

//--------------------------- Autres définitions dépendantes de <log>


#endif // __TP__GeoObject__

