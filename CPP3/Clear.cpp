/*************************************************************************
                           Clear  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Clear> (fichier Clear.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Clear.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC


//----------------------------------------------------- Méthodes publiques

bool Clear::execute() {
    
    if (myData->getDataStructure()->size() != 0) {
        clearedData = *myData->getDataStructure();
        myData->getDataStructure()->clear();
    }
    else {
        return false;
    }
    
    return true;
}

bool Clear::undo() {
    if (clearedData.size() != 0) {
        for (auto it = clearedData.begin(); it != clearedData.end(); it++) {
            myData->insertObject(it->second, true);
        }
    }
    else {
        return false;
    }
    
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Clear::Clear(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    arguments = commandTable;
} //----- Fin de Clear

Clear::~Clear ( )
// Algorithme :
//
{

} //----- Fin de ~Clear


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

