/*************************************************************************
 Line  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <Line> (fichier Line.h) ------
#if ! defined ( __TP__Line__)
#define __TP__Line__

//--------------------------------------------------- Interfaces utilisées
#include <string>
#include "GeoObject.h"

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Line>
//
//
//------------------------------------------------------------------------

class Line : public GeoObject
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques
    
    bool move(long dx, long dy);
    
    string description();
    
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    Line(string _name, Point first, Point second );
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~Line ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    //------------------------------------------------------------------ PRIVE
    
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
    Point p1;
    Point p2;
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__Line__