/*************************************************************************
 Aggregate  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <Aggregate> (fichier Aggregate.h) ------
#if ! defined ( __TP__Aggregate__)
#define __TP__Aggregate__

//--------------------------------------------------- Interfaces utilisées
#include <string>
#include <vector>
#include "GeoObject.h"

using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Aggregate>
//
//
//------------------------------------------------------------------------

class Aggregate : public GeoObject
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques

    bool move(long dx, long dy);
    
    string description();
    
    vector<GeoObject *> * getMyObjects();
    // Mode d'emploi :
    // Accesseur des objets associés a cet objet aggrege.
    // Contrat :
    //
    
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    Aggregate(string _name, vector<GeoObject*> aggregatedObject );
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~Aggregate ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    vector<GeoObject *> myObjects;
    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
   // vector<GeoObject *> myObjects;
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__Aggregate__