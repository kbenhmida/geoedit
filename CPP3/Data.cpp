/*************************************************************************
                           Data  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Data> (fichier Data.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Data.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
bool Data::instanceFlag = false;
Data* Data::single = NULL;
Data* Data::getInstance()
{
	if(! instanceFlag)
	{
		single = new Data();
		instanceFlag = true;
		return single;
	}
	else
	{
		return single;
	}
}

bool Data::insertObject(GeoObject *object, bool verify) {
    
	if (verify) {
        if (dataStructure.size() != 0) {
            
            auto it = dataStructure.find(object->getName());
            if (it != dataStructure.end()) {
                return false;
            }
            
        }
    }
    
	lastInserted = dataStructure.insert(pair<string,GeoObject*>(object->getName(),object)).first->second;
    
	return true;
}

bool Data::removeObjectByName(string nameOfObject) {
	auto it = dataStructure.find(nameOfObject);
	if (it != dataStructure.end()) {
		dataStructure.erase(it);
        //  delete it->second;
	}
	return true;
}

bool Data::addCommand(Command* myCommand) {
	if (myCommand->isFirstExecution()) {
		if (tempCommandsStack.size() != 0) {
            
			while (tempCommandsStack.size() != 0) {
				delete tempCommandsStack.top();
				tempCommandsStack.pop();
			}
            
			tempCommandsStack = stack<Command*>();
		}
	}
	myCommand->firstCall();
	commandsStack.push(myCommand);
	return true;
}

stack<Command*>* Data::getCommandsStack() {
	return &commandsStack;
}

stack<Command*>* Data::getTempCommandsStack() {
	return &tempCommandsStack;
}

bool Data::emptyTempCommandsStack() {
	tempCommandsStack = stack<Command*>();
	return true;
}

bool Data::objectsList() {
    
	for (auto it = dataStructure.begin(); it != dataStructure.end(); it++)
	{
		cout << it->second->description() << endl;
	}
    
	return true;
}

bool Data::undo() {
    
	Command *c = commandsStack.top(); // Récuperer la dernière commande
	c->undo(); // Annuler la commande
    
	if (commandsStack.size() == 1) {
		commandsStack = stack<Command*>();
	}
	else {
		commandsStack.pop(); // Enlever la derniere commande de la pile
	}
	tempCommandsStack.push(c); // Ajouter a la pile temporaire (en cas de redo)
    
	return true;
}

bool Data::redo() {
    
	Command *c = tempCommandsStack.top();
	c->execute();
    
	tempCommandsStack.pop();
	commandsStack.push(c);
    
	return true;
}

map<string, GeoObject*> * Data::getDataStructure() {
	return &dataStructure;
}

GeoObject* Data::getLastInserted() {
	return lastInserted;
}


//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Data::Data()
// Algorithme :
//
{
    
} //----- Fin de Data

Data::~Data ( )
// Algorithme :
//
{
    instanceFlag = false;
} //----- Fin de ~Data


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

