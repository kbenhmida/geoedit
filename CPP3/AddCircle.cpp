/*************************************************************************
 AddCircle  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <AddCircle> (fichier AddCircle.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include <iostream>
#include "AddCircle.h"
#include "Controller.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool AddCircle::execute() {
    
    long myX, myY, myRadius;
    if (toLong(arguments.at(2), &myX) && toLong(arguments.at(3), &myY) && toLong(arguments.at(4), &myRadius)) {
        
        Point myPoint;
        myPoint.x = myX;
        myPoint.y = myY;
        
        string name = arguments.at(1);
        Point center = myPoint;
        long radius = myRadius;
        
        // Verification de l'overflow
        if (myPoint.x > 0 && (myPoint.x+radius) < 0) {
            return false;
        }
        if (myPoint.y > 0 && (myPoint.y+radius) < 0) {
            return false;
        }
        if (myPoint.x < 0 && (myPoint.x-radius) > 0) {
            return false;
        }
        if (myPoint.y < 0 && (myPoint.y-radius) > 0) {
            return false;
        }
        
        myCircle = new Circle(name, center, radius);
        
        if(!myData->insertObject(myCircle, true)) {
            return false;
        }
     //   cout << "OK" << endl << "# New object : " << myCircle->getName() << endl;
        return true;
    }
    return false;
    
}

bool AddCircle::undo() {
    
    myData->removeObjectByName(arguments.at(1));
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

AddCircle::AddCircle(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    arguments = commandTable;
    
} //----- Fin de AddCircle

AddCircle::~AddCircle ( )
// Algorithme :
//
{
  //  delete myCircle;
} //----- Fin de ~AddCircle


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

