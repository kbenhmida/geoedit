/*************************************************************************
 AddRectangle  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <AddRectangle> (fichier AddRectangle.h) ------
#if ! defined ( __TP__AddRectangle__)
#define __TP__AddRectangle__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
#include "Rectangle.h"

using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <AddRectangle>
//
//
//------------------------------------------------------------------------

class AddRectangle : public Command
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques
        
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    AddRectangle(const AddRectangle & unAddRectangle);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    AddRectangle(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~AddRectangle ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    //------------------------------------------------------------------ PRIVE
    
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
    
    Data *myData;
    Rectangle * myRectangle;
    
    
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__AddRectangle__