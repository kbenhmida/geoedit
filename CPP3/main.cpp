//
//  main.cpp
//  CPP3
//
//  Created by Karim Benhmida on 13/01/2014.
//  Copyright (c) 2014 Karim Benhmida. All rights reserved.
//

#include <iostream>
#include <vector>
#include <map>
#include "Controller.h"
#include "AddCircle.h"
#include "GeoObject.h"
#include "Data.h"

using namespace std;

int main(int argc, const char * argv[])
{
    Controller mainController; // Initialisation du controleur
    string commandStr; // chaine de caracteres qui recevra la commande
    vector<string> commandTable; // tableau qui recevra chaque element de la commande (espace = separateur)
    
    // tant que EXIT n'a pas été appelé, le programme continue de tourner
    while (mainController.run()) {
        
        getline(cin, commandStr); // On récupère la commande tapee dans commandStr
        
        mainController.stringToTable(commandStr, &commandTable); //On parse la commande dans un tableau de string
        mainController.analyseCommand(commandTable, false); // On analyse la commande
        
        commandTable.clear(); // On vide le tableau pour recevoir une nouvelle commande
    }

    return 0;
}