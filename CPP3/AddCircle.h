/*************************************************************************
                           AddCircle  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <AddCircle> (fichier AddCircle.h) ------
#if ! defined ( __TP__AddCircle__)
#define __TP__AddCircle__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
#include "Circle.h"

using namespace std;

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <AddCircle>
//
//
//------------------------------------------------------------------------ 

class AddCircle : public Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //

    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    AddCircle(const AddCircle & unAddCircle);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    AddCircle(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~AddCircle ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    
    Data *myData;
    Circle * myCircle;
    
    
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__AddCircle__