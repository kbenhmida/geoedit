/*************************************************************************
                           GeoObject  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <GeoObject> (fichier GeoObject.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "GeoObject.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

string GeoObject::getName() {
    return name;
}

bool GeoObject::checkOverflow(Point pt, long dx, long dy) {
    
    if (pt.x > 0 && (pt.x+dx) < 0 && dx > 0) {
        return false;
    }
    if (pt.y > 0 && (pt.y+dy) < 0 && dy > 0) {
        return false;
    }
    
    if (pt.x < 0 && (pt.x+dx) > 0 && dx < 0) {
        return false;
    }
    if (pt.y < 0 && (pt.y+dy) > 0 && dy < 0) {
        return false;
    }
    
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
GeoObject::GeoObject()
// Algorithme :
//
{
    
} //----- Fin de GeoObject

GeoObject::GeoObject(string _name) : name(_name)
// Algorithme :
//
{
    
} //----- Fin de GeoObject

GeoObject::~GeoObject ( )
// Algorithme :
//
{

} //----- Fin de ~GeoObject


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

