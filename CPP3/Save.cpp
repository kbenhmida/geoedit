/*************************************************************************
 Save  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <Save> (fichier Save.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <vector>
#include <iostream>
#include <fstream>

//------------------------------------------------------ Include personnel
#include "Save.h"
#include "GeoObject.h"
#include "Aggregate.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Save::execute() {
 //   const clock_t begin_time = clock();
    vector<GeoObject*> agregats;
    
    string outFile = arguments.at(1);
    
    ofstream outStream;
    outStream.open (outFile, ofstream::out | ofstream::trunc);
    
    if(outStream.is_open()){
        
        
        for (map<string, GeoObject*>::iterator it = myData->getDataStructure()->begin(); it != myData->getDataStructure()->end(); it++) {
            if (dynamic_cast<Aggregate*>(it->second) == NULL) {
                outStream << it->second->description() << endl;
            }
            else {
                agregats.push_back(it->second);
            }
        }
        
        if (agregats.size() != 0) {
            for (int i = 0; i < agregats.size(); i++) {
                outStream << agregats[i]->description() << endl;
            }
        }
        
        outStream.close();
    }else{
      //  cout<<"ERR" << endl;
        return false;
    }
 //   cout << float(clock()- begin_time) / CLOCKS_PER_SEC << endl;
    return true;
}

bool Save::undo() {
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Save::Save(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    arguments = commandTable;
    
} //----- Fin de Save

Save::~Save ( )
// Algorithme :
//
{
    
} //----- Fin de ~Save


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées
