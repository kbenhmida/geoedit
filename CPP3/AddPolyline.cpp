/*************************************************************************
 AddPolyline  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <AddPolyline> (fichier AddPolyline.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include <iostream>
#include "AddPolyline.h"
#include "Controller.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool AddPolyline::execute() {
    
    vector<Point> parameters;
    vector<long> coordinates;
    
   // long toInsert;
    
    for(int i=2; i<(int)arguments.size();i++){
        coordinates.push_back(0);
        if(! toLong(arguments.at(i), &coordinates[i-2]) ){
            return false;
        }
    }
    
    string name = arguments.at(1);
    for(int i=0 ;i<(int)coordinates.size()/2;i++){
        Point pointEmpty;
        parameters.push_back(pointEmpty);
        parameters[i].x=coordinates[i*2];
        parameters[i].y=coordinates[i*2+1];
    }
    
    myPolyline = new Polyline(name, parameters);
        
    if(!myData->insertObject(myPolyline, true)) {
        return false;
    }
    return true;
}

bool AddPolyline::undo() {
    
    myData->removeObjectByName(arguments.at(1));
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

AddPolyline::AddPolyline(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    
    arguments = commandTable;
    
} //----- Fin de AddPolyline

AddPolyline::~AddPolyline ( )
// Algorithme :
//
{
  //  delete myPolyline;
} //----- Fin de ~AddPolyline


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

