/*************************************************************************
                           Delete  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Delete> (fichier Delete.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Delete.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Delete::execute() {
    
    deletedObjects.clear();
    affectedAggregates.clear();
    
    // On verifie que les objets existent, si oui on les ajoute a la liste d'objets a supprimer
    for (auto it = arguments.begin()+1; it != arguments.end(); it++) {
        
        auto it2 = myData->getDataStructure()->find(*it);
        
        if (it2 != myData->getDataStructure()->end()) {
            deletedObjects.push_back(it2->second);
        }
        else {
            return false;
        }
        
    }
    
    //On verifie quels sont les OA affectes par la suppression de chaque objet
    for (auto it = deletedObjects.begin(); it != deletedObjects.end(); it++) {
        
        for (auto it2 = myData->getDataStructure()->begin(); it2 != myData->getDataStructure()->end(); it2++) {
            
            // On ne verifie que pour les OA
            if (dynamic_cast<Aggregate*>(it2->second) != NULL) {
                
                Aggregate *toChange = dynamic_cast<Aggregate*>(it2->second);
                Aggregate* copy = new Aggregate(*toChange);
                
                bool isConcerned = false;
                
                for (auto it3 = copy->getMyObjects()->begin(); it3 != copy->getMyObjects()->end(); it3++) {
                    
                    
                    if ((*it)->getName() == (*it3)->getName()) {
                        
                        isConcerned = true;
                        
                        bool alreadyConcerned = false;
                        for (auto it4 = affectedAggregates.begin(); it4 != affectedAggregates.end(); it4++) {
                            if (toChange->getName() == (*it4)->getName()) {
                                alreadyConcerned = true;
                                break;
                            }
                        }
                        
                        if (!alreadyConcerned) {
                            affectedAggregates.push_back(toChange);
                        }
                        
                        copy->getMyObjects()->erase(it3);
                        
                        myData->removeObjectByName(toChange->getName());
                        myData->insertObject(copy, true);
                        
                        break;
                    }
                    
                }
                
                if (!isConcerned) {
                    delete copy;
                }
                
            }
            
        }
        
        myData->removeObjectByName((*it)->getName());
        
    }
    
    
    
    return true;
}

bool Delete::undo() {
    
    for (auto it = deletedObjects.begin(); it != deletedObjects.end(); it++) {
        myData->insertObject((*it), true);
    }
    
    for (auto it = affectedAggregates.begin(); it != affectedAggregates.end(); it++) {
        
        myData->removeObjectByName((*it)->getName());
        myData->insertObject((*it), true);
    }
    
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Delete::Delete(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    arguments = commandTable;
} //----- Fin de Delete

Delete::~Delete ( )
// Algorithme :
//
{
//    for (auto it = deletedObjects.begin(); it != deletedObjects.end(); it++) {
//        delete (*it);
//    }
} //----- Fin de ~Delete


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

