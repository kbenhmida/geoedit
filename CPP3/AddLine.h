/*************************************************************************
 AddLine  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <AddLine> (fichier AddLine.h) ------
#if ! defined ( __TP__AddLine__)
#define __TP__AddLine__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
#include "Line.h"

using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <AddLine>
//
//
//------------------------------------------------------------------------

class AddLine : public Command
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques
        
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    AddLine(const AddLine & unAddLine);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    AddLine(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~AddLine ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    //------------------------------------------------------------------ PRIVE
    
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
    
    Data *myData;
    Line * myLine;
    
    
    
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__AddLine__