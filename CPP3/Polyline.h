/*************************************************************************
 Polyline  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <Polyline> (fichier Polyline.h) ------
#if ! defined ( __TP__Polyline__)
#define __TP__Polyline__

//--------------------------------------------------- Interfaces utilisées
#include <string>
#include <vector>
#include "GeoObject.h"

using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <Polyline>
//
//
//------------------------------------------------------------------------

class Polyline : public GeoObject
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques

    bool move(long dx, long dy);
    
    string description();
    
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    Polyline(string _name, vector<Point> parameters );
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~Polyline ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    //------------------------------------------------------------------ PRIVE
    
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
    vector<Point> myPoints;
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__Polyline__