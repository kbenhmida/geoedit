/*************************************************************************
 Polyline  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <Polyline> (fichier Polyline.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Polyline.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Polyline::move(long dx, long dy) {
    
    for (auto it = myPoints.begin(); it != myPoints.end(); it++) {
        
        if (!checkOverflow(*it, dx, dy)) {
            return false;
        }
        
        it->x += dx;
        it->y += dy;
    }
    
    return true;
}

string Polyline::description() {
    stringstream command;
    command << "PL " << name;
    for(int i=0; i<(int)myPoints.size();i++){
        command <<" " << myPoints.at(i).x << " " << myPoints.at(i).y;
    }

    return command.str();
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Polyline::Polyline(string _name, vector<Point> parameters)
// Algorithme :
//
{
    name=_name;
    myPoints=parameters;
    
} //----- Fin de Polyline

Polyline::~Polyline ( )
// Algorithme :
//
{
    
} //----- Fin de ~Polyline


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

