/*************************************************************************
                           Load  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Load> (fichier Load.h) ------
#if ! defined ( __TP__Load__)
#define __TP__Load__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
#include "Controller.h"
//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Load>
//
//
//------------------------------------------------------------------------ 

class Load : public Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
//------------------------------------------------- Surcharge d'opérateurs
    
//-------------------------------------------- Constructeurs - destructeur
    Load(const Load & unLoad);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Load(vector<string> commandTable, Controller* mainC);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Load ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    Data *myData;
    Controller *mainController;
    vector<GeoObject*> insertedObjects;
};

//--------------------------- Autres définitions dépendantes de <Load>

#endif // __TP__Load__