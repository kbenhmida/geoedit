/*************************************************************************
 AddRectangle  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <AddRectangle> (fichier AddRectangle.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include <iostream>
#include "AddRectangle.h"
#include "Controller.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques


bool AddRectangle::execute() {
    
    long myFirstX, myFirstY, mySecondX, mySecondY;
    if (toLong(arguments.at(2), &myFirstX) && toLong(arguments.at(3), &myFirstY) && toLong(arguments.at(4), &mySecondX) && toLong(arguments.at(5),&mySecondY)) {
        
        Point myFirstPoint, mySecondPoint;
        myFirstPoint.x = myFirstX;
        myFirstPoint.y = myFirstY;
        mySecondPoint.x=mySecondX;
        mySecondPoint.y=mySecondY;
        
        string name = arguments.at(1);
        
        myRectangle = new Rectangle(name, myFirstPoint, mySecondPoint);
        
        if(!myData->insertObject(myRectangle, true)) {
            return false;
        }
        return true;
    }
    return false;
    
}

bool AddRectangle::undo() {
    
    myData->removeObjectByName(arguments.at(1));
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

AddRectangle::AddRectangle(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    
    arguments = commandTable;
    
} //----- Fin de AddRectangle

AddRectangle::~AddRectangle ( )
// Algorithme :
//
{
  //  delete myRectangle;
} //----- Fin de ~AddRectangle


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

