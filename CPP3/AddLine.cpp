/*************************************************************************
 AddLine  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <AddLine> (fichier AddLine.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include <iostream>
#include "AddLine.h"
#include "Controller.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool AddLine::execute() {
    
    long myFirstX, myFirstY, mySecondX, mySecondY;
    if (toLong(arguments.at(2), &myFirstX) && toLong(arguments.at(3), &myFirstY) && toLong(arguments.at(4), &mySecondX) && toLong(arguments.at(5),&mySecondY)) {
        
        Point myFirstPoint, mySecondPoint;
        myFirstPoint.x = myFirstX;
        myFirstPoint.y = myFirstY;
        mySecondPoint.x=mySecondX;
        mySecondPoint.y=mySecondY;
        
        string name = arguments.at(1);
        
        myLine = new Line(name, myFirstPoint, mySecondPoint);
        
        if(!myData->insertObject(myLine, true)) {
            return false;
        }
        return true;
    }
    return false;
    
}

bool AddLine::undo() {
    
    myData->removeObjectByName(arguments.at(1));
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

AddLine::AddLine(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    
    arguments = commandTable;
    
} //----- Fin de AddLine

AddLine::~AddLine ( )
// Algorithme :
//
{
  //  delete myLine;
} //----- Fin de ~AddLine


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

