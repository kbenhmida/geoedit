/*************************************************************************
                           Rectangle  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Rectangle> (fichier Rectangle.h) ------
#if ! defined ( __TP__Rectangle__)
#define __TP__Rectangle__

//--------------------------------------------------- Interfaces utilisées
#include <string>
#include "GeoObject.h"

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Rectangle>
//
//
//------------------------------------------------------------------------ 

class Rectangle : public GeoObject
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    

    bool move(long dx, long dy);
    
    string description();

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    Rectangle(string _name, Point first, Point second );
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Rectangle ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    Point p1;
    Point p2;
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__Rectangle__