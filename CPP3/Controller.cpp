/*************************************************************************
 Controller  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <Controller> (fichier Controller.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include <iostream>
#include "Controller.h"
#include <iostream>
#include "AddCircle.h"
#include "Save.h"
#include "AddRectangle.h"
#include "AddLine.h"
#include "AddPolyline.h"
#include "AddAggregate.h"
#include "Load.h"
#include "Clear.h"
#include "Delete.h"
#include "Move.h"

using namespace std;

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

void Controller::stringToTable(string s, vector<string> *cmdTable) {
    
    stringstream ss(s);
    istream_iterator<string> begin(ss);
    istream_iterator<string> end;
    *cmdTable = vector<string>(begin, end);
    
}

bool Controller::analyseCommand(vector<string> commandTable, bool isFromLoad) {
    
    // Rien n'a ete insere
    if (commandTable.size() == 0) {
        cout << "ERR" << endl;
    }
    else if(commandTable.at(0) == "C" && commandTable.size() == 5) {
        // cout << "# Commande cercle appelée" << endl;
        
        AddCircle *myCommand;
        
        myCommand = new AddCircle(commandTable);
        
        // On tente d'executer la commande.
        if (myCommand->execute()) {
            
            if (!isFromLoad) {
                // On tente d'ajouter la commande a l'historique des commandes sauf si elle vient d'une commande load
                if(myData->addCommand(myCommand)) {
                    // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                    cout << "OK" << endl;
                    cout << "# New object : " << commandTable.at(1) << endl;
                }
            }
            else {
                delete myCommand;
            }
            
        }
        else {
            if (!isFromLoad) {
                cout << "ERR" << endl;
            }
            delete myCommand;
            return false;
        }
        
        
    }
    else if(commandTable.at(0) == "R" && commandTable.size() == 6) {
        // cout << "# Commande rectangle appelée" << endl;
        
        AddRectangle *myCommand;
        
        myCommand = new AddRectangle(commandTable);
        
        //On tente d'exécuter la commande
        if(myCommand->execute()){
            
            if (!isFromLoad) {
                // On tente d'ajouter la commande a l'historique des commandes sauf si elle vient d'une commande load
                if(myData->addCommand(myCommand)) {
                    // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                    cout << "OK" << endl;
                    cout << "# New object : " << commandTable.at(1) << endl;
                }
            }
            else {
                delete myCommand;
            }
        }
        else {
            if (!isFromLoad) {
                cout << "ERR" << endl;
            }
            delete myCommand;
            return false;
        }
        
    }
    else if(commandTable.at(0) == "L" && commandTable.size() == 6) {
        // cout << "# Commande ligne appelée" << endl;
        
        AddLine *myCommand;
        
        myCommand = new AddLine(commandTable);
        
        //On tente d'exécuter la commande
        if(myCommand->execute()){
            
            if (!isFromLoad) {
                // On tente d'ajouter la commande a l'historique des commandes sauf si elle vient d'une commande load
                if(myData->addCommand(myCommand)) {
                    // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                    cout << "OK" << endl;
                    cout << "# New object : " << commandTable.at(1) << endl;
                }
            }
            else {
                delete myCommand;
            }
        }
        else {
            if (!isFromLoad) {
                cout << "ERR" << endl;
            }
            delete myCommand;
            return false;
        }
        
    }
    else if(commandTable.at(0) == "PL" && commandTable.size() >= 4 && (commandTable.size() % 2) == 0) {
        // cout << "# Commande polyligne appelée" << endl;
        
        AddPolyline *myCommand;
        
        myCommand = new AddPolyline(commandTable);
        
        //On tente d'exécuter la commande
        if(myCommand-> execute()){
            
            if (!isFromLoad) {
                // On tente d'ajouter la commande a l'historique des commandes sauf si elle vient d'une commande load
                if(myData->addCommand(myCommand)) {
                    // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                    cout << "OK" << endl;
                    cout << "# New object : " << commandTable.at(1) << endl;
                }
            }
            else {
                delete myCommand;
            }
        }
        else {
            if (!isFromLoad) {
                cout << "ERR" << endl;
            }
            delete myCommand;
            return false;
        }
        
    }
    else if(commandTable.at(0) == "OA" && commandTable.size() >= 3) {
        // cout << "# Commande objet agrégé appelée" << endl;
        
        AddAggregate *myCommand;
        
        myCommand = new AddAggregate(commandTable);
        
        //On tente d'exécuter la commande
        if(myCommand->execute()){
            
            if (!isFromLoad) {
                // On tente d'ajouter la commande a l'historique des commandes sauf si elle vient d'une commande load
                if(myData->addCommand(myCommand)) {
                    // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                    cout << "OK" << endl;
                    cout << "# New object : " << commandTable.at(1) << endl;
                }
            }
            else {
                delete myCommand;
            }
        }
        else {
            if (!isFromLoad) {
                cout << "ERR" << endl;
            }
            delete myCommand;
            return false;
        }
        
    }
    else if(commandTable.at(0) == "#") {
        // on ignore
    }
    else if (!isFromLoad) {
        if(commandTable.at(0) == "DELETE" && commandTable.size() >= 2) {
            // cout << "# Commande supprimer appelée" << endl;
            
            Delete *myCommand;
            
            myCommand = new Delete(commandTable);
            
            //On tente d'exécuter la commande
            if(myCommand->execute()){
                
                //On tente d'ajouter la commande à l'historique des commandes
                if(myData->addCommand(myCommand)){
                  //  cout<<"# Items in Base : " <<myData->getDataStructure()->size()<< endl;
                    cout << "OK" << endl;
                }
                
            }
            else {
                cout << "ERR" << endl;
                delete myCommand;
            }
            
        }
        else if(commandTable.at(0) == "MOVE" && commandTable.size() == 4) {
            // cout << "# Commande move appelée" << endl;
            
            Move *myCommand;
            
            myCommand = new Move(commandTable);
            
            //On tente d'exécuter la commande
            if(myCommand->execute()){
                
                // On tente d'ajouter la commande a l'historique des commandes sauf si elle vient d'une commande load
                if(myData->addCommand(myCommand)) {
                    // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                    cout << "OK" << endl;
                }
                
            }
            else {
                cout << "ERR" << endl;
                delete myCommand;
            }
            
        }
        else if(commandTable.at(0) == "LIST" && commandTable.size() == 1) {
            // cout << "# Commande list appelée" << endl;
            
            myData->objectsList();
        }
        else if(commandTable.at(0) == "UNDO" && commandTable.size() == 1) {
            // cout << "# Commande undo appelée" << endl;
            
            if (!myData->getCommandsStack()->empty()) {
                
                myData->undo();
                // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                cout << "OK" << endl;
            }
            else {
                cout << "ERR" << endl;
            }
            
        }
        else if(commandTable.at(0) == "REDO" && commandTable.size() == 1) {
            // cout << "# Commande redo appelée" << endl;
            
            if(!myData->getTempCommandsStack()->empty()) {
                
                myData->redo();
                // cout << "# Items in Base : " << myData->getDataStructure()->size() << endl;
                cout << "OK" << endl;
            }
            else {
                cout << "ERR" << endl;
            }
            
        }
        else if(commandTable.at(0) == "LOAD" && commandTable.size() == 2) {
            // cout << "# Commande load appelée" << endl;
            
            Load *myCommand;
            
            myCommand = new Load(commandTable, this);
            
            //On tente d'exécuter la commande
            if(myCommand->execute()){
                
                //On tente d'ajouter la commande à l'historique des commandes
                if(myData->addCommand(myCommand)){
                  //  cout<<"# Items in Base : " <<myData->getDataStructure()->size()<< endl;
                    cout << "OK" << endl;
                }
                
            }
            else {
                cout << "ERR" << endl;
                delete myCommand;
            }
            
        }
        else if(commandTable.at(0) == "SAVE" && commandTable.size() == 2) {
            // cout << "# Commande save appelée" << endl;
            Save *myCommand;
            myCommand = new Save(commandTable);
            
            if (myCommand->execute()) {
                cout << "OK" << endl;
            }
            else {
                cout << "ERR" << endl;
            }
            delete myCommand;
            
        }
        else if(commandTable.at(0) == "CLEAR" && commandTable.size() == 1) {
            // cout << "# Commande clear appelée" << endl;
            
            Clear *myCommand;
            
            myCommand = new Clear(commandTable);
            
            //On tente d'exécuter la commande
            if(myCommand->execute()){
                
                //On tente d'ajouter la commande à l'historique des commandes
                if(myData->addCommand(myCommand)){
                   // cout<<"# Items in Base : " <<myData->getDataStructure()->size()<< endl;
                    cout << "OK" << endl;
                }
                
            }
            else {
                cout << "ERR" << endl;
                delete myCommand;
            }
        }
        else if(commandTable.at(0) == "EXIT" && commandTable.size() == 1) {
            
            _run = false;
            
        }
        else {
            cout << "ERR" << endl;
        }
    }
    else {
        if (!isFromLoad) {
            cout << "ERR" << endl;
        }
        else {
            return false;
        }

    }
    
  //  commandTable.clear();
    
    return true;
}

bool Controller::run() {
    return _run;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Controller::Controller()
// Algorithme :
//
{
    myData = Data::getInstance();
    _run = true;
} //----- Fin de Controller

Controller::~Controller ( )
// Algorithme :
//
{
    //On libere la memoire
    Data *myData = Data::getInstance();
    
    for (auto it = myData->getDataStructure()->begin(); it != myData->getDataStructure()->end(); it++) {
        delete it->second;
    }
    myData->getDataStructure()->clear();
} //----- Fin de ~Controller


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

