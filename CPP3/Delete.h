/*************************************************************************
                           Delete  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Delete> (fichier Delete.h) ------
#if ! defined ( __TP__Delete__)
#define __TP__Delete__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
#include "Aggregate.h"
//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Delete>
//
//
//------------------------------------------------------------------------ 

class Delete : public Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    

    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    Delete(const Delete & unDelete);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Delete(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Delete ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    Data *myData;
    vector<GeoObject*> deletedObjects;
    vector<Aggregate*> affectedAggregates; // Aggregats affectés par la suppression d'un objet
};

//--------------------------- Autres définitions dépendantes de <Delete>

#endif // __TP__Delete__