/*************************************************************************
                           Move  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Move> (fichier Move.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Move.h"
#include "Aggregate.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Move::execute() {
    
    long dx, dy;
    
    if (toLong(arguments.at(2), &dx) && toLong(arguments.at(3), &dy)) {
        
        if( !getObjects(arguments.at(1)) ) {
            return false;
        }
        
        for (auto it = toMove.begin(); it != toMove.end(); it++) {
            if(it->second->move(dx, dy) == false) {
                return false;
            }
        }
        
    }
    else {
        return false;
    }
    
    return true;
}

bool Move::undo() {
    
    long dx, dy;
    
    if (toLong(arguments.at(2), &dx) && toLong(arguments.at(3), &dy)) {
        
        for (auto it = toMove.begin(); it != toMove.end(); it++) {
            it->second->move(-dx, -dy);
        }
        
    }
    
    return true;
}

bool Move::getObjects(string name) {
    
    auto it = myData->getDataStructure()->find(name);
    
    if (it != myData->getDataStructure()->end()) {
        
        // Il s'agit d'un objet agrege
        if (dynamic_cast<Aggregate*>(it->second) != NULL) {
            
            Aggregate *OA = dynamic_cast<Aggregate*>(it->second);
            
            for (auto it2 = OA->getMyObjects()->begin(); it2 != OA->getMyObjects()->end(); it2++) {
                getObjects((*it2)->getName());
            }
        }
        else {
            toMove[it->first] = it->second;
        }
        
    }
    else {
        return false;
    }
    
    return true;
}


//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Move::Move(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    arguments = commandTable;
} //----- Fin de Move

Move::~Move ( )
// Algorithme :
//
{

} //----- Fin de ~Move


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

