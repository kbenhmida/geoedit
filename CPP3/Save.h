/*************************************************************************
                           Save  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Save> (fichier Save.h) ------
#if ! defined ( __TP__Save__)
#define __TP__Save__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Save>
//
//
//------------------------------------------------------------------------ 

class Save : public Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    Save(const Save & unSave);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Save(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Save ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    Data *myData;
};

//--------------------------- Autres définitions dépendantes de <Save>

#endif // __TP__Save__