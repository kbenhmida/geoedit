/*************************************************************************
 AddPolyline  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <AddPolyline> (fichier AddPolyline.h) ------
#if ! defined ( __TP__AddPolyline__)
#define __TP__AddPolyline__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
#include "Polyline.h"

using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <AddPolyline>
//
//
//------------------------------------------------------------------------

class AddPolyline : public Command
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    AddPolyline(const AddPolyline & unAddPolyline);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    AddPolyline(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~AddPolyline ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    //------------------------------------------------------------------ PRIVE
    
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
    
    Data *myData;
    Polyline * myPolyline;
    
    
    
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__AddPolyline__