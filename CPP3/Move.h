/*************************************************************************
                           Move  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Move> (fichier Move.h) ------
#if ! defined ( __TP__Move__)
#define __TP__Move__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Move>
//
//
//------------------------------------------------------------------------ 

class Move : public Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool getObjects(string name);
    // Mode d'emploi :
    // Méthode iterative qui a partir de l'objet à déplacer va determiner
    // les eventuels objets a déplacer (lorsqu'on veut déplacer un objet
    // agrégé).
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    Move(const Move & unMove);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Move(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Move ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    Data* myData;
    map<string, GeoObject*> toMove;
};

//--------------------------- Autres définitions dépendantes de <Move>

#endif // __TP__Move__