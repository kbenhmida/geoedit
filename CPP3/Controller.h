/*************************************************************************
                           Controller  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Controller> (fichier Controller.h) ------
#if ! defined ( __TP__Controller__)
#define __TP__Controller__

//--------------------------------------------------- Interfaces utilisées
#include <map>
#include <stack>
#include <vector>
#include "GeoObject.h"
#include "Command.h"
#include "Data.h"

using namespace std;

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Controller>
//
//
//------------------------------------------------------------------------ 

class Controller
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    

    void stringToTable(string s, vector<string> *cmdTable);
    // Mode d'emploi :
    // Permet de parser un string en un vector de string (avec comme separateur l'espace)
    // Contrat :
    // Si string vide ou contenant que des espaces, le vector restera vide
    
    bool analyseCommand(vector<string> commandTable, bool isFromLoad);
    // Mode d'emploi :
    // Permet d'analyser une commande parsée et d'executer la commande correspondante
    // Contrat :
    // Si la commande parsee est vide, la méthode retourne une erreur
    
    bool run();
    
//------------------------------------------------- Surcharge d'opérateurs
    
//-------------------------------------------- Constructeurs - destructeur
    Controller(const Controller & unController);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Controller();
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Controller ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    
  //  vector<string> commandTable;
    
    bool _run; // Set to false when exit is called
    
    Data *myData;
};

//--------------------------- Autres définitions dépendantes de <Controller>

#endif // __TP__Controller__