/*************************************************************************
                           Command  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Command> (fichier Command.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>

//------------------------------------------------------ Include personnel
#include "Command.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Command::toLong(string myString, long * myLong) {
    char * error;
    long int tempLong;
    errno = 0;
    tempLong = strtol(myString.c_str(),&error,10);
    
    if(* error) {
        return false;
    }
    else if ((tempLong == LONG_MIN || tempLong == LONG_MAX) && errno == ERANGE) {
        return false;
    }
    
    *myLong = tempLong;
    
    return true;
}

bool Command::firstCall() {
    firstExecution = false;
    return true;
}

bool Command::isFirstExecution() {
    return firstExecution;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Command::Command()
// Algorithme :
//
{
    firstExecution = true;
  //  cout << "commande créee" << endl;
} //----- Fin de Command

Command::~Command ( )
// Algorithme :
//
{
  //  cout << "commande supprimée" << endl;
} //----- Fin de ~Command


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

