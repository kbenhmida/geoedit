/*************************************************************************
                           Load  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Load> (fichier Load.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
#include <fstream>
#include <sstream>
//------------------------------------------------------ Include personnel
#include "Load.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Load::execute() {
    bool shouldUndo = false;
    
    //  const clock_t begin_time = clock();
    
    if (insertedObjects.size() == 0) {
        
        vector<string> command;
        ifstream file(arguments.at(1));
        string temp;
        
        if(file.is_open()){
            while(getline(file, temp)) {
                
                mainController->stringToTable(temp, &command);
                
                if (command.at(0) == "C" || command.at(0) == "R" || command.at(0) == "L" || command.at(0) == "PL" || command.at(0) == "OA") {
                    
                    if (!mainController->analyseCommand(command, true)) {
                        shouldUndo = true;
                        command.clear();
                        break;
                    }
                    else {
                        insertedObjects.push_back(myData->getLastInserted());
                    }
                    
                }
                command.clear();
            }
            file.close();
        }
        else {
            //  cout << "ERR" << endl;
            return false;
        }
        
    }
    else {
        for (auto it = insertedObjects.begin(); it != insertedObjects.end(); it++) {
            myData->insertObject((*it), false);
        }
    }
    
    if (shouldUndo) {
        for (auto it = insertedObjects.begin(); it != insertedObjects.end(); it++) {
            myData->removeObjectByName((*it)->getName());
        }
        return false;
    }
    
    //    if (commands.size() != 0) {
    //        for (auto it = commands.begin(); it != commands.end(); it++) {
    //            mainController->analyseCommand(*it, true);
    //        }
    //    }
    
    // Je crée un vecteur de vecteur de string
    // ifstream tout ça, je lis ligne par ligne, et pour chaque ligne je met dans un vect de string
    // puis je verifie que le nom n'existe pas déjà dans ma structure (déjà codé), si il existe, return false, sinon ne rien faire
    // ensuite je fais une boucle sur mon vecteur de vect de string et pour chaque item, je fais mainController->analyseCommande(iterateur)
    
    //   cout << float(clock()- begin_time) / CLOCKS_PER_SEC << endl;
    return true;
}

bool Load::undo() {
    
    if (insertedObjects.size() != 0) {
        for (auto it = insertedObjects.begin(); it != insertedObjects.end(); it++) {
            myData->removeObjectByName((*it)->getName());
        }
    }
    else {
        return false;
    }
    
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Load::Load(vector<string> commandTable, Controller* mainC)
// Algorithme :
//
{
    myData = Data::getInstance();
    arguments = commandTable;
    
    mainController = mainC;
} //----- Fin de Load

Load::~Load ( )
// Algorithme :
//
{
    for (auto it = insertedObjects.begin(); it != insertedObjects.end(); it++) {
        delete *it;
    }
} //----- Fin de ~Load


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

