/*************************************************************************
 Aggregate  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <Aggregate> (fichier Aggregate.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Aggregate.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Aggregate::move(long dx, long dy) {
    
    for (auto it = myObjects.begin(); it != myObjects.end(); it++) {
        if ((*it)->move(dx, dy) == false) {
            return false;
        }
    }
    
    return true;
}

string Aggregate::description() {
    stringstream command;
    command << "OA " << name;
    for(int i=0; i<(int)myObjects.size();i++){
        command <<" " << myObjects.at(i)->getName();
    }
    
    return command.str();
}

vector<GeoObject *> * Aggregate::getMyObjects() {
    return &myObjects;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Aggregate::Aggregate(string _name, vector<GeoObject*> aggregatedObject)
// Algorithme :
//
{
    name=_name;
    myObjects=aggregatedObject;
    
} //----- Fin de Aggregate

Aggregate::~Aggregate ( )
// Algorithme :
//
{
    
} //----- Fin de ~Aggregate


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

