/*************************************************************************
                           Data  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Data> (fichier Data.h) ------
#if ! defined ( __TP__Data__)
#define __TP__Data__

//--------------------------------------------------- Interfaces utilisées
#include <map>
#include <stack>
#include <vector>
#include "GeoObject.h"
#include "Command.h"

using namespace std;
//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Data>
//
//
//------------------------------------------------------------------------ 

class Data
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    
  	static Data* getInstance();
    // Mode d'emploi :
    // Retourne l'unique instance de cette classe (Singleton)
    // Contrat :
    //
    
	bool insertObject(GeoObject *object, bool verify);
    // Mode d'emploi :
    // Insère un objet dans la structure de données. Si verify est vrai,
    // vérifie qu'il n'existe pas avant de l'ajouter
    // Contrat :
    //
	bool removeObjectByName(string nameOfObject);
    // Mode d'emploi :
    // Supprime un objet de la structure de données à partir de son nom.
    // Contrat :
    //
    bool objectsList();
    // Mode d'emploi :
    // Appelle le descripteur de tous les objets de la structure.
    // Contrat :
    //
    
    map<string, GeoObject*>* getDataStructure();
    // Mode d'emploi :
    // Accesseur de la structure de données.
    // Contrat :
    //
	
    bool addCommand(Command *myCommand);
    // Mode d'emploi :
    // Ajoute une commande à la pile de commandes.
    // Contrat :
    //
    
	stack<Command*>* getCommandsStack();
    // Mode d'emploi :
    // Accesseur de la pile de commandes.
    // Contrat :
    //
	stack<Command*>* getTempCommandsStack();
    // Mode d'emploi :
    // Accesseur de la pile temporaire de commandes.
    // Contrat :
    //
    
	bool emptyTempCommandsStack();
    // Mode d'emploi :
    // Vide la pile temporaire de commandes.
    // Contrat :
    //
    
	bool undo();
    // Mode d'emploi :
    // Annule la derniere commande de la pile de commandes
    // et l'insere dans la pile temporaire.
    // Contrat :
    //
    bool redo();
    // Mode d'emploi :
    // Reexecute la derniere commande annulee et l'insere dans la pile de commandes.
    // Contrat :
    //
    
	GeoObject* getLastInserted();
    // Mode d'emploi :
    // Accesseur du dernier element geometrique insere
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    ~Data();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées
	Data();
    // Mode d'emploi :
    // Constructeur protégé pour ne pas permettre de créer
    // plusieurs instances (Singleton)
    // Contrat :
    //
//----------------------------------------------------- Attributs protégés
    static bool instanceFlag;
	static Data *single;
    
    stack<Command*> commandsStack;
	stack<Command*> tempCommandsStack;
	map<string, GeoObject*> dataStructure;
	GeoObject* lastInserted;
};

//--------------------------- Autres définitions dépendantes de <Data>

#endif // __TP__Data__