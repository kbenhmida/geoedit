/*************************************************************************
                           Rectangle  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Rectangle> (fichier Rectangle.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Rectangle.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Rectangle::move(long dx, long dy) {
    
    if (!checkOverflow(p1, dx, dy) || !checkOverflow(p2, dx, dy)) {
        return false;
    }
    
    p1.x += dx;
    p1.y += dy;
    
    p2.x += dx;
    p2.y += dy;
    
    return true;
}

string Rectangle::description() {
    stringstream command;
    command << "R " << name << " " << p1.x << " " << p1.y << " "<<p2.x<<" "<<p2.y;
    return command.str();
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Rectangle::Rectangle(string _name, Point first, Point second )
// Algorithme :
//
{
    name=_name;
    p1=first;
    p2=second;
    
} //----- Fin de Rectangle

Rectangle::~Rectangle ( )
// Algorithme :
//
{

} //----- Fin de ~Rectangle


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

