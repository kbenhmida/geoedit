/*************************************************************************
 AddAggregate  -  description
 -------------------
 début                : 28 nov. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Réalisation de la classe <AddAggregate> (fichier AddAggregate.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include <iostream>
#include "AddAggregate.h"
#include "Controller.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool AddAggregate::execute() {
    
    map<string, GeoObject*>* myObjectMap = myData->getDataStructure();
    vector<GeoObject*> myObjects;
    
    for(int i=2; i<(int)arguments.size();i++){
        map<string, GeoObject*>::iterator it=myObjectMap->find(arguments[i]);
        if( it==myObjectMap->end()){
           // cout<<"ERR"<<endl;
            return false;
        }else{
            GeoObject* itemEmpty;
            myObjects.push_back(itemEmpty);
            myObjects[i-2]=it->second;
        }
    }
    
    string name = arguments.at(1);
    
    myAggregate = new Aggregate(name, myObjects);
    
    if(!myData->insertObject(myAggregate, true)) {
        return false;
    }
    return true;
}

bool AddAggregate::undo() {
    
    myData->removeObjectByName(arguments.at(1));
    return true;
}

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur

AddAggregate::AddAggregate(vector<string> commandTable)
// Algorithme :
//
{
    myData = Data::getInstance();
    
    arguments = commandTable;
    
} //----- Fin de AddAggregate

AddAggregate::~AddAggregate ( )
// Algorithme :
//
{
  //  delete myAggregate;
} //----- Fin de ~AddAggregate


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

