/*************************************************************************
 AddAggregate  -  Description
 -------------------
 début                : 28 oct. 2013
 copyright            : (C) 2013 par Karim
 *************************************************************************/

//---------- Interface de la classe <AddAggregate> (fichier AddAggregate.h) ------
#if ! defined ( __TP__AddAggregate__)
#define __TP__AddAggregate__

//--------------------------------------------------- Interfaces utilisées
#include <map>
#include "Command.h"
#include "Data.h"
#include "Aggregate.h"

using namespace std;

//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//------------------------------------------------------------------------
// Rôle de la classe <AddAggregate>
//
//
//------------------------------------------------------------------------

class AddAggregate : public Command
{
    //----------------------------------------------------------------- PUBLIC
    
public:
    //----------------------------------------------------- Méthodes publiques
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //
    //------------------------------------------------- Surcharge d'opérateurs
    
    //-------------------------------------------- Constructeurs - destructeur
    AddAggregate(const AddAggregate & unAddAggregate);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //
    
    AddAggregate(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    virtual ~AddAggregate ();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    //------------------------------------------------------------------ PRIVE
    
protected:
    //----------------------------------------------------- Méthodes protégées
    
    //----------------------------------------------------- Attributs protégés
    
    Data *myData;
    Aggregate * myAggregate;
    
    
    
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__AddAggregate__