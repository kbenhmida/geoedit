/*************************************************************************
                           Command  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Command> (fichier Command.h) ------
#if ! defined ( __TP__Command__)
#define __TP__Command__

#include <vector>
#include <string>

using namespace std;

//--------------------------------------------------- Interfaces utilisées

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Command>
//
//
//------------------------------------------------------------------------ 

class Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    
    
    bool toLong(string myString, long * myLong);
    // Mode d'emploi :
    // Converti un string en long si possible avec verification d'overflow
    // Contrat :
    //

    virtual bool execute() = 0;
    // Mode d'emploi :
    // Execute la commande
    // Contrat :
    //
    
    virtual bool undo() = 0;
    // Mode d'emploi :
    // Annule la commande
    // Contrat :
    //
    
    bool firstCall();
    // Mode d'emploi :
    // Méthode appelée lors de la première execution d'une commande
    // (lorsque la commande provient directement de l'utilisateur).
    // Contrat :
    //
    
    bool isFirstExecution();
    // Mode d'emploi :
    // Accesseur de firstExecution qui indique si la commande provient
    // directement de l'utilisateur ou bien d'un redo.
    // Contrat :
    //

    
//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
  //  Command(const Command & unCommand);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Command();
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Command ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    vector<string> arguments;
    bool firstExecution;
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__Command__