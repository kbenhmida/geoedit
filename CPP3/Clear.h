/*************************************************************************
                           Clear  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Clear> (fichier Clear.h) ------
#if ! defined ( __TP__Clear__)
#define __TP__Clear__

//--------------------------------------------------- Interfaces utilisées
#include "Command.h"
#include "Data.h"
//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Clear>
//
//
//------------------------------------------------------------------------ 

class Clear : public Command
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    
    
    bool execute();
    // Mode d'emploi :
    //
    // Contrat :
    //
    
    bool undo();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    Clear(const Clear & unClear);
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    Clear(vector<string> commandTable);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Clear ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    Data *myData;
    map<string, GeoObject*> clearedData;
};

//--------------------------- Autres définitions dépendantes de <Clear>

#endif // __TP__Clear__