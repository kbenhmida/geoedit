/*************************************************************************
                           Circle  -  Description
                             -------------------
    début                : 28 oct. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Interface de la classe <Circle> (fichier Circle.h) ------
#if ! defined ( __TP__Circle__)
#define __TP__Circle__

//--------------------------------------------------- Interfaces utilisées
#include "GeoObject.h"

//------------------------------------------------------------- Constantes 

//------------------------------------------------------------------ Types 

//------------------------------------------------------------------------ 
// Rôle de la classe <Circle>
//
//
//------------------------------------------------------------------------ 

class Circle : public GeoObject
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques    
    
    bool move(long dx, long dy);
    
    string description();

//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
    Circle(string name, Point center, long radius);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~Circle ();
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE 

protected:
//----------------------------------------------------- Méthodes protégées

//----------------------------------------------------- Attributs protégés
    
    Point center;
    long radius;
    
};

//--------------------------- Autres définitions dépendantes de <log>

#endif // __TP__Circle__