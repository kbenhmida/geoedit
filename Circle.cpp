/*************************************************************************
                           Circle  -  description
                             -------------------
    début                : 28 nov. 2013
    copyright            : (C) 2013 par Karim
*************************************************************************/

//---------- Réalisation de la classe <Circle> (fichier Circle.cpp) -------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système

//------------------------------------------------------ Include personnel
#include "Circle.h"


//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques

bool Circle::move(long dx, long dy) {
    
    if ((center.x+radius) > 0 && (center.x+radius+dx) < 0 && dx > 0) {
        return false;
    }
    if ((center.y+radius) > 0 && (center.y+radius+dy) < 0 && dy > 0) {
        return false;
    }
    
    if ((center.x+radius) < 0 && (center.x+radius+dx) > 0 && dx < 0) {
        return false;
    }
    if ((center.y+radius) < 0 && (center.y+radius+dy) > 0 && dy < 0) {
        return false;
    }
    
    center.x += dx;
    center.y += dy;
    return true;
}

 string Circle::description() {
     stringstream command;
     command << "C " << name << " " << center.x << " " << center.y << " " << radius;
     return command.str();
}


//------------------------------------------------- Surcharge d'opérateurs

//-------------------------------------------- Constructeurs - destructeur
Circle::Circle(string _name, Point _center, long _radius) : GeoObject(_name)
// Algorithme :
//
{
    center = _center;
    radius = _radius;
    
} //----- Fin de Circle

Circle::~Circle ( )
// Algorithme :
//
{

} //----- Fin de ~Circle


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

